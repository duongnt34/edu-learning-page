
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
// import VueVideoPlayer from 'vue-video-player'
import videojs from 'video.js'
import videojscontribhls from "videojs-contrib-hls"
import videojscontribmediasources from "videojs-contrib-media-sources"
import 'video.js/dist/video-js.css'
import Unicon from 'vue-unicons'
import { uniConstructor, uniCarWash } from 'vue-unicons/src/icons'
// import VueRouter from 'vue-router';
// import Routes from './routes'
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Unicon.add([uniConstructor, uniCarWash])
Vue.use(Unicon)
Vue.use(videojscontribhls)
// Vue.use(videojscontribmediasources)
// Vue.use(VueRouter);
// Vue.use(VueVideoPlayer, /* {
//     options: global default options,
//     events: global videojs events
//   } */
// )

// const router = new VueRouter({
//     routes: Routes
// });

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('learning-top', require('./components/learning-page/learning-top/LearningTop.vue').default);
Vue.component('learning-video', require('./components/learning-page/learning-video/LearningVideo.vue').default);
Vue.component('learning-bottom', require('./components/learning-page/learning-bottom/LearningBottom.vue').default);
Vue.component('learning-side-nav', require('./components/learning-page/learning-top/LearningSideNav.vue').default);
Vue.component('learning-discussion', require('./components/learning-page/learning-top/LearningDiscussion.vue').default);
Vue.component('learning-note-modal', require('./components/learning-page/modules/LearningNoteModal.vue').default);
// Vue.component('video-player', require('./components/VideoPlayer.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const learningPage = new Vue({
    el: '#learningPage',
    data:{
        title: "Duong",
    },
    // router: router,
    mounted(){
        $(".learning-top").hide();
        $(".learning-bottom").hide();
        $("#learningPage").hover(
            function(){
                $(".learning-top").show();
                $(".learning-bottom").show();

            },
            function(){
                $(".learning-top").hide();
                $(".learning-bottom").hide();
            }
        )
    },
});

var Component = videojs.getComponent('Component');
var TitleBar = videojs.extend(Component, {
    constructor: function(player, options){
        Component.apply(this.arguments);

        if(options.text){
            this.updateTextContent(options.text);
        }
    },
    createEl: function(){
        return videojs.createEl('div',{
            className: 'vjs-title-bar'
        })
    },
    updateTextContent: function(text){
        if (typeof text !== 'string') {
            text = 'Title Unknown';
        }

        videojs.emptyEl(this.el());
        videojs.appendContent(this.el(), text);
    }
})

videojs.registerComponent('TitleBar', TitleBar);


// setTimeout(function(){
//     var playVideo = _this.player.play();
//     if (playVideo !== undefined) {
//         playVideo.then(_ => {
//         // Autoplay started!
//     }).catch(error => {
//         // Autoplay was prevented.
//         // Show a "Play" button so that user can start playback.
//       });
//     }
//   }, 2000);