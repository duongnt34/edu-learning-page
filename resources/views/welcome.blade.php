@php

use ColorThief\ColorThief;
use App\CustomClasses\ColorFound;



$dominantColors = ColorThief::getPalette('https://www.dropbox.com/s/ylzy7ild5tlsy9k/parquet_boards_wood_texture_stripes_50459_1080x1920.jpg?raw=1', 5);
$colorFound = new ColorFound;
$colorNames = array();

@endphp
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
    </head>
    <body>
        <h1>Get Prominent Colors</h1>
    @php
        foreach($dominantColors as $color){
            $xColor = $colorFound->getName($color);
            array_push($colorNames, $xColor);

        }
        // print_r($colorFound->getName($dominantColor)); 
        foreach($colorNames as $colorName){
            echo $colorName;
        }
    @endphp
</body>
</html>