@php
    $lesson = $lesson->toJson();
    // dd($lesson);
    $course_id = 1;
    $sections_json = json_encode($sections);
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no,viewport-fit=cover">
    
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Scripts -->
        {{-- <script type="text/javascript" src="//player.wowza.com/player/latest/wowzaplayer.min.js"></script> --}}


        {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
        
        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('css/learning-page.css') }}">
        <!-- Fonts -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link href="https://vjs.zencdn.net/7.4.1/video-js.css" rel="stylesheet">
    
</head>
<body>
    <div id="learningPage">
        {{-- <div class="learning-top">
            <learning-top :lesson="{{$lesson}}"></learning-top>
        </div> --}}
        <div class="learning-video">
            <learning-side-nav :sections="{{$sections_json}}" :course_id="{{$course_id}}"></learning-side-nav>
            <learning-discussion></learning-discussion>
            <learning-video :lesson="{{$lesson}}" :sections="{{$sections_json}}"></learning-video>
        </div>
        {{-- <div class="learning-bottom">
            <learning-bottom></learning-bottom>
        </div> --}}


        <div class="modal fade" id="noteModal" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <learning-note-modal></learning-note-modal>
            </div>
        </div>



    </div>
    <script src="{{ asset('js/learning-page.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/videojs-contrib-hls@5.15.0/es5/videojs-contrib-hls.min.js"></script>
    {{-- <script type="text/javascript">
        WowzaPlayer.create('playerElement',
            {
            "license":"PLAY2-dNuxm-JeMfe-YH3zT-h9J9x-uZTPZ",
            "title":"",
            "description":"",
            "sourceURL":"http://192.168.2.90:1935/vod/_definst_/lacnhaucophaimuondoi.mp4/playlist.m3u8",
            "autoPlay":false,
            "volume":"75",
            "mute":false,
            "loop":false,
            "audioOnly":false,
            "uiShowQuickRewind":true,
            "uiQuickRewindSeconds":"30"
            }
        );
        </script> --}}
</body>
</html>