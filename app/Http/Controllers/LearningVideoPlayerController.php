<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lesson;

class LearningVideoPlayerController extends Controller
{
    public function index($courseId, $lessonId){
        $sections = array(
            array(
                "id" => "1",
                "section_title" => "Part 1: V-Pop",
                "lessons" =>  array(
                    array("id"=>1,"lesson_title" => "Lạc Nhau Có Phải Muôn Đời", "url"=>"/lacnhaucophaimuondoi.mp4"),
                    array("id"=>2,"lesson_title" => "Người Con Gái Ta Thương", "url"=>"/nguoicongaitathuong.mp4"),
                    array("id"=>7,"lesson_title" => "Cảm Giác Lúc Ấy Sẽ Ra Sao", "url"=>"/camgiaclucayserasao.mp4"),
                    array("id"=>8,"lesson_title" => "Nếu Anh Đi", "url"=>"/neuanhdi.mp4"),
                    array("id"=>13,"lesson_title" => "Chạy Ngay Đi", "url"=>"/neuanhdi.mp4"),)  
                                    
            ),
            array(
                "id" => "2",
                "section_title" => "Part 2: Nhac Do",
                "lessons" =>  array(array("id"=>3,"lesson_title" => "Chiếc Gậy Trường Sơn", "url"=>"/chiecgaytruongson.mp4"),
                                array("id"=>4,"lesson_title" => "Lá Xanh", "url"=>"/laxanh.mp4"),)  
            ),
            array(
                "id" => "3",
                "section_title" => "Part 3: Nhac Vang",
                "lessons" =>  array(array("id"=>5,"lesson_title" => "Không Giờ Rồi", "url"=>"/khonggioroi.mp4"),
                                array("id"=>6,"lesson_title" => "Duyên Phận", "url"=>"/duyenphan.mp4"),) 
            ),
            array(
                "id" => "4",
                "section_title" => "Part 4: K-Pop",
                "lessons" => array(
                    array("id"=>9,"lesson_title" => "Kill The Love", "url"=>"/lacnhaucophaimuondoi.mp4"),
                    array("id"=>10,"lesson_title" => "Boy With Luv", "url"=>"/nguoicongaitathuong.mp4"),
                    array("id"=>11,"lesson_title" => "Bang Bang Bang", "url"=>"/camgiaclucayserasao.mp4"),
                    array("id"=>12,"lesson_title" => "Face", "url"=>"/neuanhdi.mp4"),)   
            ),
            
        );
        $lesson = Lesson::find($lessonId);
        // dd($lesson);
        return view('learning', [
            'sections'=>$sections,
            'lesson' => $lesson,

        
        ]);
    }
}
